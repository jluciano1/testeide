package diagram.teste.rest;

import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;

import org.springframework.http.*;
import org.springframework.beans.factory.annotation.*;

import java.util.*;

import diagram.teste.entity.*;
import diagram.teste.business.*;



/**
 * Controller para expor serviços REST de Peca
 * 
 * @author local
 * @version 1.0
 * @generated
 **/
@RestController
@RequestMapping(value = "/api/rest/diagram/teste/Peca")
public class PecaREST {

    /**
     * Classe de negócio para manipulação de dados
     * 
     * @generated
     */
    @Autowired
    @Qualifier("PecaBusiness")
    private PecaBusiness pecaBusiness;

    /**
     * @generated
     */
      @Autowired
      @Qualifier("CarroBusiness")
      private CarroBusiness carroBusiness;
    /**
     * @generated
     */
      @Autowired
      @Qualifier("CarroPecaBusiness")
      private CarroPecaBusiness carroPecaBusiness;

    /**
     * Serviço exposto para novo registro de acordo com a entidade fornecida
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.POST)
    public Peca post(@Validated @RequestBody final Peca entity) throws Exception {
        return pecaBusiness.post(entity);
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade fornecida
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.PUT)
    public Peca put(@Validated @RequestBody final Peca entity) throws Exception {
        return pecaBusiness.put(entity);
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade e id fornecidos
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Peca put(@PathVariable("id") final java.lang.String id, @Validated @RequestBody final Peca entity) throws Exception {
        return pecaBusiness.put(entity);
    }

    /**
     * Serviço exposto para remover a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") java.lang.String id) throws Exception {
        pecaBusiness.delete(id);
    }


  /**
   * NamedQuery list
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  )    
  public  HttpEntity<PagedResources<Peca>> listParams (Pageable pageable, PagedResourcesAssembler assembler){
      return new ResponseEntity<>(assembler.toResource(pecaBusiness.list(pageable   )), HttpStatus.OK);    
  }

  /**
   * OneToMany Relationship GET
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  , value="/{instanceId}/CarroPeca")    
  public HttpEntity<PagedResources<CarroPeca>> findCarroPeca(@PathVariable("instanceId") java.lang.String instanceId, Pageable pageable, PagedResourcesAssembler assembler) {
    return new ResponseEntity<>(assembler.toResource(pecaBusiness.findCarroPeca(instanceId,  pageable )), HttpStatus.OK);
  }

  /**
   * OneToMany Relationship DELETE 
   * @generated
   */  
  @RequestMapping(method = RequestMethod.DELETE
  , value="/{instanceId}/CarroPeca/{relationId}")    
  public void deleteCarroPeca(@PathVariable("relationId") java.lang.String relationId) throws Exception {
    this.carroPecaBusiness.delete(relationId);
  }
  
  /**
   * OneToMany Relationship PUT
   * @generated
   */  
  @RequestMapping(method = RequestMethod.PUT
  , value="/{instanceId}/CarroPeca/{relationId}")
  public CarroPeca putCarroPeca(@Validated @RequestBody final CarroPeca entity, @PathVariable("relationId") java.lang.String relationId) throws Exception {
	return this.carroPecaBusiness.put(entity);
  }  
  
  /**
   * OneToMany Relationship POST
   * @generated
   */  
  @RequestMapping(method = RequestMethod.POST
  , value="/{instanceId}/CarroPeca")
  public CarroPeca postCarroPeca(@Validated @RequestBody final CarroPeca entity, @PathVariable("instanceId") java.lang.String instanceId) throws Exception {
	Peca peca = this.pecaBusiness.get(instanceId);
	entity.setPeca(peca);
	return this.carroPecaBusiness.post(entity);
  }   


  /**
   * ManyToMany Relationship GET
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  ,value="/{instanceId}/Carro")
  public HttpEntity<PagedResources<Carro>> listCarro(@PathVariable("instanceId") java.lang.String instanceId,  Pageable pageable, PagedResourcesAssembler assembler ) {
    return new ResponseEntity<>(assembler.toResource(pecaBusiness.listCarro(instanceId,  pageable )), HttpStatus.OK); 
  }

  /**
   * ManyToMany Relationship POST
   * @generated
   */  
  @RequestMapping(method = RequestMethod.POST
  ,value="/{instanceId}/Carro")
  public Peca postCarro(@Validated @RequestBody final Carro entity, @PathVariable("instanceId") java.lang.String instanceId) throws Exception {
      CarroPeca newCarroPeca = new CarroPeca();

      Peca instance = this.pecaBusiness.get(instanceId);

      newCarroPeca.setCarro(entity);
      newCarroPeca.setPeca(instance);
      
      this.carroPecaBusiness.post(newCarroPeca);

      return newCarroPeca.getPeca();
  }   

  /**
   * ManyToMany Relationship DELETE
   * @generated
   */  
  @RequestMapping(method = RequestMethod.DELETE
  ,value="/{instanceId}/Carro/{relationId}")
  public void deleteCarro(@PathVariable("instanceId") java.lang.String instanceId, @PathVariable("relationId") java.lang.String relationId) {
	  this.pecaBusiness.deleteCarro(instanceId, relationId);
  }  



    /**
     * Serviço exposto para recuperar a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Peca get(@PathVariable("id") java.lang.String id) throws Exception {
        return pecaBusiness.get(id);
    }
}
