package diagram.teste.rest;

import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;

import org.springframework.http.*;
import org.springframework.beans.factory.annotation.*;

import java.util.*;

import diagram.teste.entity.*;
import diagram.teste.business.*;



/**
 * Controller para expor serviços REST de CarroPeca
 * 
 * @author local
 * @version 1.0
 * @generated
 **/
@RestController
@RequestMapping(value = "/api/rest/diagram/teste/CarroPeca")
public class CarroPecaREST {

    /**
     * Classe de negócio para manipulação de dados
     * 
     * @generated
     */
    @Autowired
    @Qualifier("CarroPecaBusiness")
    private CarroPecaBusiness carroPecaBusiness;


    /**
     * Serviço exposto para novo registro de acordo com a entidade fornecida
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.POST)
    public CarroPeca post(@Validated @RequestBody final CarroPeca entity) throws Exception {
        return carroPecaBusiness.post(entity);
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade fornecida
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.PUT)
    public CarroPeca put(@Validated @RequestBody final CarroPeca entity) throws Exception {
        return carroPecaBusiness.put(entity);
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade e id fornecidos
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public CarroPeca put(@PathVariable("id") final java.lang.String id, @Validated @RequestBody final CarroPeca entity) throws Exception {
        return carroPecaBusiness.put(entity);
    }

    /**
     * Serviço exposto para remover a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") java.lang.String id) throws Exception {
        carroPecaBusiness.delete(id);
    }




    /**
     * Serviço exposto para recuperar a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public CarroPeca get(@PathVariable("id") java.lang.String id) throws Exception {
        return carroPecaBusiness.get(id);
    }
}
