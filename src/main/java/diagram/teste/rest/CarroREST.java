package diagram.teste.rest;

import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;

import org.springframework.http.*;
import org.springframework.beans.factory.annotation.*;

import java.util.*;

import diagram.teste.entity.*;
import diagram.teste.business.*;



/**
 * Controller para expor serviços REST de Carro
 * 
 * @author local
 * @version 1.0
 * @generated
 **/
@RestController
@RequestMapping(value = "/api/rest/diagram/teste/Carro")
public class CarroREST {

    /**
     * Classe de negócio para manipulação de dados
     * 
     * @generated
     */
    @Autowired
    @Qualifier("CarroBusiness")
    private CarroBusiness carroBusiness;

    /**
     * @generated
     */
      @Autowired
      @Qualifier("PecaBusiness")
      private PecaBusiness pecaBusiness;
    /**
     * @generated
     */
      @Autowired
      @Qualifier("CarroPecaBusiness")
      private CarroPecaBusiness carroPecaBusiness;

    /**
     * Serviço exposto para novo registro de acordo com a entidade fornecida
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.POST)
    public Carro post(@Validated @RequestBody final Carro entity) throws Exception {
        return carroBusiness.post(entity);
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade fornecida
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.PUT)
    public Carro put(@Validated @RequestBody final Carro entity) throws Exception {
        return carroBusiness.put(entity);
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade e id fornecidos
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Carro put(@PathVariable("id") final java.lang.String id, @Validated @RequestBody final Carro entity) throws Exception {
        return carroBusiness.put(entity);
    }

    /**
     * Serviço exposto para remover a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") java.lang.String id) throws Exception {
        carroBusiness.delete(id);
    }


  /**
   * NamedQuery list
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  )    
  public  HttpEntity<PagedResources<Carro>> listParams (Pageable pageable, PagedResourcesAssembler assembler){
      return new ResponseEntity<>(assembler.toResource(carroBusiness.list(pageable   )), HttpStatus.OK);    
  }

  /**
   * NamedQuery listByAttribute_01
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  , value="/listByAttribute_01/{attribute_01}")    
  public  HttpEntity<PagedResources<Carro>> listByAttribute_01Params (@PathVariable("attribute_01") java.lang.String attribute_01, Pageable pageable, PagedResourcesAssembler assembler){
      return new ResponseEntity<>(assembler.toResource(carroBusiness.listByAttribute_01(attribute_01, pageable   )), HttpStatus.OK);    
  }

  /**
   * OneToMany Relationship GET
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  , value="/{instanceId}/CarroPeca")    
  public HttpEntity<PagedResources<CarroPeca>> findCarroPeca(@PathVariable("instanceId") java.lang.String instanceId, Pageable pageable, PagedResourcesAssembler assembler) {
    return new ResponseEntity<>(assembler.toResource(carroBusiness.findCarroPeca(instanceId,  pageable )), HttpStatus.OK);
  }

  /**
   * OneToMany Relationship DELETE 
   * @generated
   */  
  @RequestMapping(method = RequestMethod.DELETE
  , value="/{instanceId}/CarroPeca/{relationId}")    
  public void deleteCarroPeca(@PathVariable("relationId") java.lang.String relationId) throws Exception {
    this.carroPecaBusiness.delete(relationId);
  }
  
  /**
   * OneToMany Relationship PUT
   * @generated
   */  
  @RequestMapping(method = RequestMethod.PUT
  , value="/{instanceId}/CarroPeca/{relationId}")
  public CarroPeca putCarroPeca(@Validated @RequestBody final CarroPeca entity, @PathVariable("relationId") java.lang.String relationId) throws Exception {
	return this.carroPecaBusiness.put(entity);
  }  
  
  /**
   * OneToMany Relationship POST
   * @generated
   */  
  @RequestMapping(method = RequestMethod.POST
  , value="/{instanceId}/CarroPeca")
  public CarroPeca postCarroPeca(@Validated @RequestBody final CarroPeca entity, @PathVariable("instanceId") java.lang.String instanceId) throws Exception {
	Carro carro = this.carroBusiness.get(instanceId);
	entity.setCarro(carro);
	return this.carroPecaBusiness.post(entity);
  }   


  /**
   * ManyToMany Relationship GET
   * @generated
   */
  @RequestMapping(method = RequestMethod.GET
  ,value="/{instanceId}/Peca")
  public HttpEntity<PagedResources<Peca>> listPeca(@PathVariable("instanceId") java.lang.String instanceId,  Pageable pageable, PagedResourcesAssembler assembler ) {
    return new ResponseEntity<>(assembler.toResource(carroBusiness.listPeca(instanceId,  pageable )), HttpStatus.OK); 
  }

  /**
   * ManyToMany Relationship POST
   * @generated
   */  
  @RequestMapping(method = RequestMethod.POST
  ,value="/{instanceId}/Peca")
  public Carro postPeca(@Validated @RequestBody final Peca entity, @PathVariable("instanceId") java.lang.String instanceId) throws Exception {
      CarroPeca newCarroPeca = new CarroPeca();

      Carro instance = this.carroBusiness.get(instanceId);

      newCarroPeca.setPeca(entity);
      newCarroPeca.setCarro(instance);
      
      this.carroPecaBusiness.post(newCarroPeca);

      return newCarroPeca.getCarro();
  }   

  /**
   * ManyToMany Relationship DELETE
   * @generated
   */  
  @RequestMapping(method = RequestMethod.DELETE
  ,value="/{instanceId}/Peca/{relationId}")
  public void deletePeca(@PathVariable("instanceId") java.lang.String instanceId, @PathVariable("relationId") java.lang.String relationId) {
	  this.carroBusiness.deletePeca(instanceId, relationId);
  }  



    /**
     * Serviço exposto para recuperar a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Carro get(@PathVariable("id") java.lang.String id) throws Exception {
        return carroBusiness.get(id);
    }
}
