package diagram.teste.dao;

import diagram.teste.entity.*;



import org.springframework.stereotype.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.*;
import org.springframework.data.repository.query.*;
import org.springframework.transaction.annotation.*;



/**
 * Realiza operação de Create, Read, Update e Delete no banco de dados.
 * Os métodos de create, edit, delete e outros estão abstraídos no JpaRepository
 * 
 * @see org.springframework.data.jpa.repository.JpaRepository
 * 
 * @generated
 */
@Repository("PecaDAO")
@Transactional(transactionManager="diagram.teste-TransactionManager")
public interface PecaDAO extends JpaRepository<Peca, java.lang.String> {

  /**
   * Obtém a instância de Peca utilizando os identificadores
   * 
   * @param id
   *          Identificador 
   * @return Instância relacionada com o filtro indicado
   * @generated
   */    
  @Query("SELECT entity FROM Peca entity WHERE entity.id = :id")
  public Peca findOne(@Param(value="id") java.lang.String id);

  /**
   * Remove a instância de Peca utilizando os identificadores
   * 
   * @param id
   *          Identificador 
   * @return Quantidade de modificações efetuadas
   * @generated
   */    
  @Modifying
  @Query("DELETE FROM Peca entity WHERE entity.id = :id")
  public void delete(@Param(value="id") java.lang.String id);

  /**
   * Lista com paginação de acordo com a NamedQuery
   * 
   * @generated
   */
  @Query("select p from Peca p")
  public Page<Peca> list ( Pageable pageable );
  

  /**
   * OneToMany Relation
   * @generated
   */
  @Query("SELECT entity FROM CarroPeca entity WHERE entity.peca.id = :id")
  public Page<CarroPeca> findCarroPeca(@Param(value="id") java.lang.String id,  Pageable pageable );



  /**
   * ManyToOne Relation
   * @generated
   */
  @Query("SELECT entity.carro FROM CarroPeca entity WHERE entity.peca.id = :id")
  public Page<Carro> listCarro(@Param(value="id") java.lang.String id,  Pageable pageable);

    /**
     * ManyToOne Relation Delete
     * @generated
     */
    @Modifying
    @Query("DELETE FROM CarroPeca entity WHERE entity.peca.id = :instanceId AND entity.carro.id = :relationId")
    public int deleteCarro(@Param(value="instanceId") java.lang.String instanceId, @Param(value="relationId") java.lang.String relationId);




}