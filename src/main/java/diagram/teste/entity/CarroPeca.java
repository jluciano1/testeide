package diagram.teste.entity;

import java.io.*;
import javax.persistence.*;
import java.util.*;
import javax.xml.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;




/**
 * Classe que representa a tabela CARROPECA
 * @generated
 */
@Entity
@Table(name = "\"CARROPECA\""


)
@XmlRootElement
public class CarroPeca implements Serializable {

  /**
   * UID da classe, necessário na serialização 
   * @generated
   */
  private static final long serialVersionUID = -2121529941l;
  
  /**
   * @generated
   */
  @Id
    
  @Column(name = "id", insertable=true, updatable=true)
  private java.lang.String id = UUID.randomUUID().toString().toUpperCase();
  
  /**
   * @generated
   */
  @ManyToOne
  @JoinColumn(name="fk_carro", referencedColumnName = "id", insertable=true, updatable=true)
  private Carro carro;
  
  /**
   * @generated
   */
  @ManyToOne
  @JoinColumn(name="fk_peca", referencedColumnName = "id", insertable=true, updatable=true)
  private Peca peca;
  
  
  /**
   * Construtor
   * @generated
   */
  public CarroPeca(){
  }

  
  /**
   * Obtém id
   * @param id id
   * return id
   * @generated
   */
  public java.lang.String getId(){
    return this.id;
  }
  
  /**
   * Define id
   * @param id id
   * @generated
   */
  public CarroPeca setId(java.lang.String id){
    this.id = id;
    return this;
  }
  
  /**
   * Obtém carro
   * @param carro carro
   * return carro
   * @generated
   */
  public Carro getCarro(){
    return this.carro;
  }
  
  /**
   * Define carro
   * @param carro carro
   * @generated
   */
  public CarroPeca setCarro(Carro carro){
    this.carro = carro;
    return this;
  }
  
  /**
   * Obtém peca
   * @param peca peca
   * return peca
   * @generated
   */
  public Peca getPeca(){
    return this.peca;
  }
  
  /**
   * Define peca
   * @param peca peca
   * @generated
   */
  public CarroPeca setPeca(Peca peca){
    this.peca = peca;
    return this;
  }
  
  /**
   * @generated
   */
  @Override
  public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((id == null) ? 0 : id.hashCode());

        return result;
    }
  
  /**
   * @generated
   */ 
  @Override
    public boolean equals(Object obj) {
    
      if(this == obj)
        return true;
      
      if(obj == null)
        return false;
      
      if(!(obj instanceof CarroPeca))
        return false;
      
      CarroPeca other = (CarroPeca)obj;
      
    if(this.id == null && other.id != null)
        return false;
      else if(!this.id.equals(other.id))
        return false;
  

      return true;
      
  }
}
