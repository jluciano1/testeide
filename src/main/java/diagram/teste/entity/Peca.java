package diagram.teste.entity;

import java.io.*;
import javax.persistence.*;
import java.util.*;
import javax.xml.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;




/**
 * Classe que representa a tabela PECA
 * @generated
 */
@Entity
@Table(name = "\"PECA\""


)
@XmlRootElement
public class Peca implements Serializable {

  /**
   * UID da classe, necessário na serialização 
   * @generated
   */
  private static final long serialVersionUID = 2485542l;
  
  /**
   * @generated
   */
  @Id
    
  @Column(name = "id", insertable=true, updatable=true)
  private java.lang.String id = UUID.randomUUID().toString().toUpperCase();
  
  /**
   * @generated
   */
  @Column(name = "attribute_01", nullable = false, unique = false, insertable=true, updatable=true)
  private java.lang.String attribute_01;
  
  
  /**
   * Construtor
   * @generated
   */
  public Peca(){
  }

  
  /**
   * Obtém id
   * @param id id
   * return id
   * @generated
   */
  public java.lang.String getId(){
    return this.id;
  }
  
  /**
   * Define id
   * @param id id
   * @generated
   */
  public Peca setId(java.lang.String id){
    this.id = id;
    return this;
  }
  
  /**
   * Obtém attribute_01
   * @param attribute_01 attribute_01
   * return attribute_01
   * @generated
   */
  public java.lang.String getAttribute_01(){
    return this.attribute_01;
  }
  
  /**
   * Define attribute_01
   * @param attribute_01 attribute_01
   * @generated
   */
  public Peca setAttribute_01(java.lang.String attribute_01){
    this.attribute_01 = attribute_01;
    return this;
  }
  
  /**
   * @generated
   */
  @Override
  public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((id == null) ? 0 : id.hashCode());

        return result;
    }
  
  /**
   * @generated
   */ 
  @Override
    public boolean equals(Object obj) {
    
      if(this == obj)
        return true;
      
      if(obj == null)
        return false;
      
      if(!(obj instanceof Peca))
        return false;
      
      Peca other = (Peca)obj;
      
    if(this.id == null && other.id != null)
        return false;
      else if(!this.id.equals(other.id))
        return false;
  

      return true;
      
  }
}
