package diagram.teste.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import diagram.teste.dao.*;
import diagram.teste.entity.*;



/**
 * Classe que representa a camada de negócios de PecaBusiness
 * 
 * @generated
 **/
@Service("PecaBusiness")
public class PecaBusiness {


    /**
     * Instância da classe PecaDAO que faz o acesso ao banco de dados
     * 
     * @generated
     */
    @Autowired
    @Qualifier("PecaDAO")
    protected PecaDAO repository;

    // CRUD

    /**
     * Serviço exposto para novo registro de acordo com a entidade fornecida
     * 
     * @generated
     */
    public Peca post(final Peca entity) throws Exception {
      // begin-user-code  
      // end-user-code  
        repository.save(entity);
      // begin-user-code  
      // end-user-code  
      return entity;
    }

    /**
     * Serviço exposto para recuperar a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    public Peca get(java.lang.String id) throws Exception {
      // begin-user-code  
      // end-user-code        
       Peca result = repository.findOne(id);
      // begin-user-code  
      // end-user-code        
      return result;
    }

    /**
     * Serviço exposto para salvar alterações de acordo com a entidade fornecida
     * 
     * @generated
     */
    public Peca put(final Peca entity) throws Exception {
      // begin-user-code  
      // end-user-code
        repository.saveAndFlush(entity);
      // begin-user-code  
      // end-user-code        
      return entity;
    }

    /**
     * Serviço exposto para remover a entidade de acordo com o id fornecido
     * 
     * @generated
     */
    public void delete( java.lang.String id) throws Exception {
      // begin-user-code  
      // end-user-code        
      repository.delete(id);
      // begin-user-code  
      // end-user-code        
    }

    // CRUD
    
  /**
   * Lista com paginação de acordo com a NamedQuery
   * 
   * @generated
   */
  public Page<Peca> list ( Pageable pageable ){
    // begin-user-code  
    // end-user-code        
    Page<Peca> result = repository.list (  pageable );
    // begin-user-code  
    // end-user-code        
    return result;
  }
    
    

  /**
   * @generated modifiable
   * OneToMany Relation
   */  
  public Page<CarroPeca> findCarroPeca(java.lang.String id,  Pageable pageable) {
      // begin-user-code
      // end-user-code  
      Page<CarroPeca> result = repository.findCarroPeca(id,  pageable );
      // begin-user-code  
      // end-user-code        
      return result;    
  }



  /**
   * @generated modifiable
   * ManyToMany Relation
   */  
  public Page<Carro> listCarro(java.lang.String id,  Pageable pageable ) {
      // begin-user-code
      // end-user-code  
      Page<Carro> result = repository.listCarro(id,  pageable );
      // begin-user-code
      // end-user-code
      return result;            
  }
  
  /**
   * @generated modifiable
   * ManyToMany Relation
   */    
  public int deleteCarro(java.lang.String instanceId, java.lang.String relationId) {
      // begin-user-code
      // end-user-code  
      int result = repository.deleteCarro(instanceId, relationId);
      // begin-user-code
      // end-user-code  
      return result;  
  }
}